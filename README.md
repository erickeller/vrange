# README #

with help of Vagrant setup a precise 32 bit range environment.

### Precondition ###

* install vagrant: http://www.vagrantup.com/downloads.html
* install ansible:

```
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update && sudo apt-get install ansible
```

* install vagrant lxc plugin: https://github.com/fgrehm/vagrant-lxc

### Startup ###

```
vagrant up --provider lxc --provision
vagrant ssh -- -Y
firefox
```

connect to range, use citrix icaclient,... have fun!

### Notice ###

* replace the provider with something else if you want/need